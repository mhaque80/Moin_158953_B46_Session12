<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/17/2017
 * Time: 6:27 PM
 */

namespace App;

echo __NAMESPACE__;
class Student extends Person{
    private $studentID;

    public function setStudentID($studentID)
    {
        $this->studentID = $studentID;
    }

    public function getStudentID()
    {
        return $this->studentID;
    }
}